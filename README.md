# ddd-starter

Quick Start:

- __Round 1:__ Do start with the spring guide for messaging: https://spring.io/guides/gs/messaging-rabbitmq/#initial

Create product(-management):

- __Round 2:__ Rename to product service, add product aggregate (with id, name, description)
- __Round 3:__ Add REST controller (read/write)
- __Round 4:__ Add message publisher

Create product-view:

- __Round 5:__ Create consumer service
- __Round 6:__ Add message consumer, deserialize message into reference data
- __Round 7:__ Add REST controller to receive queries, Remove read access at REST controller in product-management

## Slides

- https://slides.com/dataduke/2018-11-27-ddd


## Examples

__DDD Examples__

- https://github.com/mploed/ddd-aggregate-example
- https://github.com/mploed/ddd-strategic-design-spring-boot
- https://github.com/mploed/ddd-with-spring

__CQRS & Event Sourcing Examples__

- https://github.com/ddd-by-examples/all-things-cqrs​
- https://github.com/ddd-by-examples/event-source-cqrs-sample
- https://github.com/uweschaefer/es-basics
